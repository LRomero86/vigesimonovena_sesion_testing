const assert = require('assert');
const { markAsUntransferable } = require('worker_threads');

describe('#test', function() {
    
    describe('#indexOf()', function () {
        it('debería retornar -1 cuando el valor no está en el array', function() {
            assert.strictEqual([1,2,3].indexOf(5), -1);
        });
    });

    describe('Calculos aritméticos', () => {
        
        before(() => {
            //Iniciarlizar bd, variables...
            console.log('Este código se ejecuta antes de cualquier tests');
        });

        after(() => {
            //Limpiar bd, tablas, arrays o variables
            console.log('Este código se ejecuta después de todos los tests');
        });

        //describe permite agrupar pruebas similares en el mismo bloque
        describe('En este test se prueba una suma', () => {
            //inicializar variable suma
            let suma = 0;
            beforeEach(() => {
                console.log('Asignar valor inicial a la variable');
                suma = 2;
            });
            it('debería retornar 5', () => {
                suma += 3;
                //let n1 = 2;
                //let n2 = 3;
                assert.strictEqual(suma, 5);
            });
        });

        describe('En este test se prueba una resta', () => {
            //inicializar variable suma
            let resta = 10;
            beforeEach(() => {
                console.log('Asignar valor inicial a la variable');
                
            });
            
            it('debería retornar 7', () => {
                resta -= 3;
                //let n1 = 2;
                //let n2 = 3;
                assert.strictEqual(resta, 7);
            });

            it('debería retornar 4', () => {
                resta -= 3;
                //let n1 = 2;
                //let n2 = 3;
                assert.strictEqual(resta, 4);
            });
        });
    });
});