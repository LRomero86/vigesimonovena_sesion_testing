const assert = require('chai').assert;
const calculadora = require('../app/calculadora');

describe('test de calculadora usando ASSERT interface de chai', () => {

    describe('Verificar función sumar', () => {
   
        it('deberia retornar un valor igual a 3', () => {
            const result = calculadora.sumar(1,2);
            assert.equal(result, 3);
        });
        
        it('debería ser de tipo número', () => {
            const result = calculadora.sumar(1,2);
            console.log('tipo de valor es: ', typeof(result));
            assert.typeOf(result, 'number');
        });
   
    });

});