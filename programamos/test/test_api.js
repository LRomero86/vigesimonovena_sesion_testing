const assert = require('assert');
const fetch = require('node-fetch');
const { isTypedArray } = require('util/types');

describe('Probando API', () => {
    
    it('API responde 200', async () => {
        await fetch('https://jsonplaceholder.typicode.com/todos/1')
        .then(response => {
            console.log(response.status);
            assert.strictEqual(response.status, 200);
        });
    });

    it('API responde 404', async () => {
        await fetch('https://jsonplaceholder.typicode.com/todos/1zxcv')
        .then(response => {
            console.log(response.status);
            assert.strictEqual(response.status, 404);
        });
    });

    it('El user_id de la respuesta debe ser 1', async () => {
        await fetch('https://jsonplaceholder.typicode.com/todos/1')
        .then(response.json())
        .then(json => {
            assert.strictEqual(json.userId, 1);
        });
    });

    it('Acá se prueba openapiweather', async () => {
        city_name = 'Río Grande';
        let url = `https://api.openweathermap.org/data/2.5/weather?q=${city_name}&appid=dbe454eb3d8d927bc799d9bc44259969`;
        await fetch('https://api.openweathermap.org/data/2.5/weather?q=${city_name}&appid=dbe454eb3d8d927bc799d9bc44259969')
        .then(response => {
            console.log('APIWEATHER OK', response.status);
            assert.equal(response.status, 200);
        });
        
        
    });

    it('Acá se prueba openapiweather ERROR', async () => {
        city_name = '###';
        let url = `https://api.openweathermap.org/data/2.5/weather?q=${city_name}&appid=dbe454eb3d8d927bc799d9bc44259969`;
        await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city_name}&appid=dbe454eb3d8d927bc799d9bc44259969`)
        .then(response => {
            console.log('APIWEATHER ERROR OK', response.status);
            assert.equal(response.status, 404);
        });
        
        
    });

});